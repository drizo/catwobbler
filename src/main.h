/// <summary>
/// CatWobbler
/// A cat toy with a randomly vibrating motor.
/// 
/// Uses a Attiny85 (Digistump) controller with a 3.7v battery, motor and push button
/// The Attiny controlls the play and sleep cycles of the motor as well as the battery state.
/// Since a 3.7v LiPo (260 mhA) is used we want to use a little as possible battery power
/// and need to check the battery voltage during every step to prevent deep discharge
///
///     -   All unecessary features/pins will be switched off to preserve energy
///     -   The Attiny clock speed will be set to its lowest to save energy/work with 3.7v
///     -   Watchdog and PowerDown sleep will be activated for even lower power consumption
///     -   Every 8 sec (longest period possible) or on button press the Attiny will wake and check for state changes
///     -   The push button is used to switch the toy on and off.
///     -   The toy will be fully enclosed, therefore feedback to the user will be provided via the vibrating motor
/// </summary>
/// 
/// <author>Richard Deininger</autor>


/// Power Saving
#define adc_disable()       (ADCSRA &= ~(1<<ADEN))  // disable ADC (before power-off)
#define adc_enable()        (ADCSRA |=  (1<<ADEN))  // re-enable ADC
#define CLOCK_SPEED_125KHZ  7                       // lowest clockspeed possible

#define SEQ_WAIT_SCALER     2   // global scaler for sequence delay times

#define MIN_BAT_mV  3300L   // Low power switch off threshold
#define LED_PIN     1       // led pin (only used for debugging, set MOTORPIN = LED_PIN to seed led flash instead of motor vibrating)
#define MOTORPIN    2       // pin for vibration motor
#define INT_PIN     0       // pin for push button/interrupt

/// State definitions for main logic
#define LOWPOWEROFF 0   //  power to low, complete shutdown until battery switch
#define OFF         1   //  toy turned off (will still check battery and interrupts)
#define POWERDOWN   2   //  prev to off, to provide feedback to user
#define STARTING    3   //  prev to play, to provide feedback to user
#define PLAY        4   //  Randomly plays sequences for x cycles and will then WAIT
#define WAIT        5   //  Waiting x cycles and will then PLAY
#define BUTTONPRESS 6   //  Not going back to sleep to check button press and debouce

/// PLAY and WAIT min and max cycles
// one sleep cycle is 8 seconds... therfore 7.5 cycles = 60 seconds (1 Min) 
// (without the sequence playing in between)
#define MIN_PLAY_CYCLES   8     // = ~1 min
#define MAX_PLAY_CYCLES   45    // = 6 mins
#define MIN_WAIT_CYCLES   225   // = 30 mins
#define MAX_WAIT_CYCLES   900   // = 2 hrs

/// Sequences
#define SEQ_CNT     6   // Number of sequences in the sequence array
#define WAITFLAG    128 // we use the highest bit for flagging if we should wait

// Important!!! use 0 to show that the sequence ends!!!
// start sequence for user feedback
static byte startingSeq[] = {   1, WAITFLAG | 1, 
                                1, WAITFLAG | 1,
                                0 };
// power down sequence for user feedback
static byte powerDownSeq[] = {  2, WAITFLAG | 1, 
                                2, WAITFLAG | 1, 
                                2, WAITFLAG | 1 , 0 };

// Plays sequences to choose from
static byte sequences[SEQ_CNT][25] = 
{        
    // RampUp
    {   1, WAITFLAG | 2, 
        2, WAITFLAG | 2, 
        3, WAITFLAG | 2 , 
        0 },
    // RampDown
    {   3, WAITFLAG | 2, 
        2, WAITFLAG | 2, 
        1, WAITFLAG | 2, 
        0 },            
    // Seq 3
    {   3, WAITFLAG | 2, 
        3, WAITFLAG | 2, 
        3, WAITFLAG | 2, 
        0 },
    // Seq 4
    {   1, WAITFLAG | 1, 
        1, WAITFLAG | 1, 
        3, WAITFLAG | 2 , 
        1, WAITFLAG | 1, 
        1, WAITFLAG | 1, 
        0 },
    //Seq 5
    {   3, WAITFLAG | 3, 
        1, WAITFLAG | 1, 
        3, WAITFLAG | 3 , 
        1, WAITFLAG | 1, 
        0 },
    //Seq 6
    {   4, WAITFLAG | 3, 
        1, WAITFLAG | 3, 
        4, WAITFLAG | 3 , 
        1, WAITFLAG | 3, 
        0 },
};

/// Global variables
volatile static unsigned long lastBtnPress = 0;         // last time button was pressed
volatile static bool intButton = false;                 // button interrupt (pressed/not pressed)
volatile static byte SystemState = 0;                   // current system/main logic state
volatile static unsigned long remainingStateCycles = 0; // cicles to remain in PLAY/WAIT cycle

/// <summary>Disables/Pulls down all not needed pins to save energy</summary>
void DisableAllPins();

/// <summary>Checks if the battery still provides enought voltage to work without damaging it (using a 3.7v LiPo)</summary>
/// <returns>boolean: true if battery has still enought power / false if not</returns> 
bool CheckBatteryState();

/// <summary>Changes the speed of the CPU to save energy/work with a 3.7v LiPo battery</summary>
/// <param name="speed">The new clock speed of the CPI</param>   
volatile void setClockSpeed(byte speed);

/// <summary>Reads the voltage provided via vin/5V pin in milli volt</summary>
/// <returns>long: milli volt read from th 5v/vin pin</returns> 
long readVcc();

/// <summary>Plays the provided sequence with the motor on the defined motor pin</summary>
/// <param name="sequence">
/// A byte array containing the play/pause sequence of the motor terminated with '0' value    
/// See: sequences and WAITFLAG definition
/// </param>  
void PlaySequence(byte sequence[]);

/// <summary>Randomly select a sequence (or none) and plays it</summary>
void PlayRandomSequence();

/// <summary>
/// Will disable all function for absolutly lowest power consumtion.
/// Will also disable battery check.
/// Can only be reset by battery change
/// </summary>
void LowBatteryPowerDown();
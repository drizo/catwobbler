#include <Arduino.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include "main.h" 


// the setup routine runs once when you press reset:
void setup() 
{       
  // disable all unscessary power functions
  DisableAllPins();       // disable unused pins
  cli();                  // disable interrupts
  setClockSpeed(CLOCK_SPEED_125KHZ);
  adc_disable();          // Disable Analog-to-Digital Converter

  // setup watchdog to wake everx 8 seconds
  wdt_reset();            // Watchdog reset
  wdt_enable(WDTO_8S);    // Watchdog enable Options: 15MS, 30MS, 60MS, 120MS, 250MS, 500MS, 1S, 2S, 4S, 8S
  WDTCR |= _BV(WDIE);     // Interrupts watchdog enable

  // enable Pin 0 as interrupt for button and wdt wake up
  GIMSK |= _BV(PCIE);     // Enable Pin Change Interrupts
  PCMSK |= _BV(PCINT0);   // Use PB0o7kzt as interrupt pin
 
  
  sei();                  // enable interrupts
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Sleep Mode: max
  SystemState = OFF;
}

// the loop routine runs over and over again forever:
void loop() 
{
  if (SystemState > LOWPOWEROFF) // are we in comlete shutdown !?
  {
    // First thing to do is check if the battery is ok
    if (!CheckBatteryState())
    {    
      // Completly shudown everything until new battery is provided
      LowBatteryPowerDown();
    }

    // Countdown the cycles we want to stay in this state
    if (remainingStateCycles > 0)
    {
      remainingStateCycles--;
    }
    // introduces some noise
    randomSeed(millis());

    // main logic switch
    switch (SystemState)
    {
      case OFF:
          // Do nothing, go back to sleep
          remainingStateCycles = 0;
        break;
      case POWERDOWN:
          // give haptic feedback to user and switch off
          PlaySequence(powerDownSeq);
          SystemState = OFF;
        break;
      case STARTING:
          // give haptic feedback to user set state cycles and switch to play mode
          PlaySequence(startingSeq);
          remainingStateCycles = random(MIN_PLAY_CYCLES, MAX_PLAY_CYCLES);
          SystemState = PLAY;
        break;
      case PLAY:        
          // Play a random sequence
          PlayRandomSequence();
          // check if we cycled enought
          if (remainingStateCycles <= 0)
          {          
            // set new state cycles and switch to wait mode
            remainingStateCycles = random(MIN_WAIT_CYCLES, MAX_WAIT_CYCLES);
            SystemState = WAIT;
          }
        break;
      case WAIT:
          // wait, do nothing but count the remaining state cycles 
          if (remainingStateCycles <= 0)
          {
            // set new state cycles and switch to play mode
            remainingStateCycles = random(MIN_PLAY_CYCLES, MAX_PLAY_CYCLES);
            SystemState = PLAY;
          }
        break;    
      default:
        break;
    }
  } 
  // only go back to sleep if the button is currently not pressed
  if (SystemState < BUTTONPRESS)
  {
    // go back night night
    sleep_enable();
    sleep_cpu();
  }
}


void PlayRandomSequence()
{
  if (random(10) < 6) // only play sequence in 60% of cases
  {
    int rounds= random(1,4);
    for (int i = 0; i < rounds; i++)
    {
      // select a random sequence from the list an play it
      PlaySequence(sequences[random(SEQ_CNT)]);
    }
  }
}

void PlaySequence(byte sequence[])
{
  byte i = 0;
  while (sequence[i] != 0) // run through the sequence array until we hit a 0
  {
    bool play = ((sequence[i] & WAITFLAG) == 0); // extract the wait/play flag
    byte stepTime = (sequence[i] & ~WAITFLAG) * SEQ_WAIT_SCALER; // get the step time
    
    if (play) // Activate motor if defined
    {
      digitalWrite(MOTORPIN, HIGH);
    }
    // Wait for the sepcified ammount of time
    delay(stepTime);
    digitalWrite(MOTORPIN, LOW);
    // next step in sequence
    i++;
  }
  
}

void DisableAllPins()
{
  // Power Saving setup
  for (byte i = 0; i < 6; i++) 
  {
    pinMode(i, INPUT);      // Set all ports as INPUT to save energy
    digitalWrite (i, LOW);  // Pulldown pin to save energy
  }

  // enable only needed pins
  pinMode(INT_PIN, INPUT_PULLUP);
  pinMode(MOTORPIN, OUTPUT);
}


void LowBatteryPowerDown()
{
  for (byte i = 0; i < 6; i++) 
  {
    pinMode(i, INPUT);      // Set all ports as INPUT to save energy
    digitalWrite (i, LOW);  // Pulldown pin to save energy
  }
  cli();
  adc_disable();   
  SystemState = LOWPOWEROFF;
  remainingStateCycles = 0;
}

bool CheckBatteryState()
{
  // enable internal ADC converter for battery check only
  adc_enable();
  // read the voltage provided via vin/5V pin 
  // if lower the defined value battery voltage is to low
  bool battOK = (readVcc() > MIN_BAT_mV);
  // disable internal ADC to save power
  adc_disable();
  return battOK;
}

volatile void setClockSpeed(byte speed) 
{
  CLKPR = 0b10000000; // enable clock change
  CLKPR = speed;      // set clock to provided new speed
}

ISR(PCINT0_vect)
{
  if (SystemState > LOWPOWEROFF) // are we in comlete shutdown !?
  {
    // activate button press state to hinder sleeping
    SystemState = BUTTONPRESS;
    // check if button was pressed (pulldown)
    intButton = (digitalRead(INT_PIN) == LOW);
    if (intButton)
    {
      // if changed to pressed mark the time
      lastBtnPress = millis();
    } else 
    {
      // if the button was released check how log it was pressed prev.
      unsigned long elapsed = millis() - lastBtnPress;
      
      // long press = powerdown, short press = starting
      if (elapsed > 2)
      {
        SystemState = POWERDOWN;
      } else 
      {
        SystemState = STARTING;
      } 
    }
  }
}


ISR (WDT_vect) 
{
  if (SystemState > LOWPOWEROFF) // are we in comlete shutdown !?
  {
    WDTCR |= _BV(WDIE);
  }
}


long readVcc() 
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif 
 
  delay(1); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH 
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

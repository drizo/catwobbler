#include <Arduino.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
 
#define adc_disable() (ADCSRA &= ~(1<<ADEN)) // disable ADC (before power-off)
#define adc_enable()  (ADCSRA |=  (1<<ADEN)) // re-enable ADC

#define CLOCK_SPEED_125KHZ 7

#define MIN_BAT_mV  3300L
#define MOTORPIN    2
#define LED_PIN     1
#define INT_PIN     0

#define OFF   0
#define PLAY  1
#define WAIT  2

volatile static unsigned long lastBtnHigh = 0;
volatile static bool intButton = false;
volatile static int SystemState = 0;

// the setup routine runs once when you press reset:
void setup() 
{       
  DisableAllPins();
           
  cli();
  setClockSpeed(CLOCK_SPEED_125KHZ);
  adc_disable();          // Disable Analog-to-Digital Converter

 
  wdt_reset();            // Watchdog reset
  wdt_enable(WDTO_1S);    // Watchdog enable Options: 15MS, 30MS, 60MS, 120MS, 250MS, 500MS, 1S, 2S, 4S, 8S
  WDTCR |= _BV(WDIE);     // Interrupts watchdog enable

  GIMSK |= _BV(PCIE);     // Enable Pin Change Interrupts
  PCMSK |= _BV(PCINT0);   // Use PB3 as interrupt pin
 
  sei();                  // enable interrupts
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Sleep Mode: max
  
}

// the loop routine runs over and over again forever:
void loop() 
{
  if (!CheckBatteryState())
  {
    SystemState = OFF;
  }

  switch (SystemState)
  {
    case PLAY:
        digitalWrite(LED_PIN, HIGH);
      break;
    case WAIT:
      break;
    case OFF:
      digitalWrite(LED_PIN, LOW);
      break;
    default:
      break;
  }
  sleep_enable();
  sleep_cpu();
}

void DisableAllPins()
{
  // Power Saving setup
  for (byte i = 0; i < 6; i++) 
  {
    pinMode(i, INPUT);      // Set all ports as INPUT to save energy
    digitalWrite (i, LOW);  //
  }

  pinMode(INT_PIN, INPUT_PULLUP);
  pinMode(MOTORPIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  
}

bool CheckBatteryState()
{
   adc_enable();
   bool battOK = (readVcc() > MIN_BAT_mV);
   adc_disable();
   return battOK;
}

volatile void setClockSpeed(byte speed) 
{
  CLKPR = 0b10000000; // enable clock change
  CLKPR = speed;
}

ISR(PCINT0_vect)
{
  intButton = (digitalRead(INT_PIN) == LOW);

  if (intButton)
  {
    lastBtnHigh = millis();
    SystemState = WAIT;
  } else 
  {
    unsigned long elapsed = millis() - lastBtnHigh;
    
    if (elapsed > 2)
    {
      SystemState = OFF;
    } else 
    {
       SystemState = PLAY;
    } 
  }
}

ISR (WDT_vect) 
{
  WDTCR |= _BV(WDIE);
}


long readVcc() 
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif 
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH 
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}
